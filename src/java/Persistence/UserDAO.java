/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import Model.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class UserDAO extends BaseDAO{
   
    private static final Logger LOG = Logger.getLogger(UserDAO.class.getName());

    public ArrayList<User> getAll() {
        PreparedStatement stmt = null;
        ArrayList<User> users = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from users2");
            ResultSet rs = stmt.executeQuery();
            users = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setName(rs.getString("name"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setAge(rs.getInt("age"));

                users.add(user);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return users;
    }

        public User get(int id) {

        PreparedStatement stmt = null;
        User user = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from users2 where id = ? ");
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();
            rs.next();

            user = new User();

            user.setId(rs.getInt("id"));
            user.setName(rs.getString("name"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            user.setAge(rs.getInt("age"));

            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return user;
    }

    
    public void insert(User user) {
        PreparedStatement stmt = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("INSERT INTO users2(name, email, password, age) VALUES(?, ? ,? , ?)");
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getEmail());
            stmt.setString(3, user.getPassword());
            stmt.setInt(4, user.getAge());
                              
            stmt.execute();
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return;        
    }
    
    
    public void delete(int id) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "DELETE FROM users2"
                + " WHERE id = ?"
        );
        stmt.setInt(1, id);
        stmt.execute();
        this.disconnect();
    }    
    
    public void update(User user) throws SQLException {
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "UPDATE users2 SET "
                        + "name = ?, "
                        + "email = ?, "
                        + "password = ?, "
                        + "age = ? "
                        + " WHERE id = ? "
        );
        stmt.setString(1, user.getName());
        stmt.setString(2, user.getEmail());
        stmt.setString(3, user.getPassword());
        stmt.setInt(4, user.getAge());
        stmt.setLong(5, user.getId());
        LOG.info("id : " + user.getId());
        stmt.execute();
        this.disconnect();
        return;
    }    
    
}
