/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Model.Book;
import Persistence.BookDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author alumno
 */
public class BookController extends BaseController {
    
    public void index() {
        LOG.info("BookController->index()");
        BookDAO bookDAO;     
        
        //objeto persistencia
        bookDAO = new BookDAO();
        ArrayList<Book> books = null;

        //leer datos de la persistencia
        synchronized (bookDAO) {
            books = bookDAO.getAll();
        }
        request.setAttribute("bookList", books);
        String name = "index";
        LOG.info("En ModuleController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/book/index.jsp");        
    }
    
    public void show(String idString) throws SQLException {
        LOG.info("BookController->show(" + idString +")");
        Book book;
        //objeto persistencia
        BookDAO bookDAO;             
        bookDAO = new BookDAO();
        book = bookDAO.get(Integer.parseInt(idString));        
        request.setAttribute("book", book);
        dispatch("/WEB-INF/book/show.jsp");        
    }
    public void create() {
        dispatch("/WEB-INF/book/create.jsp");        
        LOG.info("BookController->create");
    }
    public void store() throws SQLException {
        //crear objeto
        Book book = loadFromRequest();

//        Book book = new Book();
//        book.setTitle(request.getParameter("title"));
//        book.setPublisher(request.getParameter("publisher"));
//        book.setPages(Integer.parseInt(request.getParameter("pages")));
//        book.setYear(Integer.parseInt(request.getParameter("year")));
        
        //objeto persistencia
        BookDAO bookDAO;             
        bookDAO = new BookDAO();
        
        //guardar objeto
        bookDAO.insert(book);
        LOG.info("BookController->create");
        
        //redirigir
        redirect(contextPath + "/book/index");
        
    }
    public void delete(String idString) throws SQLException {
        LOG.info("BookController->index()");
        //objeto persistencia
        BookDAO bookDAO;             
        bookDAO = new BookDAO();
        bookDAO.delete(Integer.parseInt(idString));
        //redirigir
        redirect(contextPath + "/book/index");

    }

    public void edit(String idString) throws SQLException {
        LOG.info("idString");
        long id = toId(idString);
        BookDAO  bookDAO = new BookDAO();
        Book book = bookDAO.get((int) id);
        request.setAttribute("book", book);
        dispatch("/WEB-INF/book/edit.jsp");

    }

    public void update() throws IOException, SQLException {
        BookDAO  bookDAO = new BookDAO();
        Book book = loadFromRequest();
        //crear objeto
        book.setId(Integer.parseInt(request.getParameter("id")));
        
        bookDAO.update(book);
        response.sendRedirect(contextPath + "/book/index");
        return;
    }

    private Book loadFromRequest()
    {
        Book book = new Book();
        book.setTitle(request.getParameter("title"));
        book.setPublisher(request.getParameter("publisher"));
        book.setPages(Integer.parseInt(request.getParameter("pages")));
        book.setYear(Integer.parseInt(request.getParameter("year")));
        return book;
    }
            
            

    private static final Logger LOG = Logger.getLogger(BookController.class.getName());

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
