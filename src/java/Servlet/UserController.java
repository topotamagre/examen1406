/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;


import Model.Book;
import Model.User;
import Persistence.BookDAO;
import Persistence.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author usuario
 */
public class UserController extends BaseController {
    private UserDAO userDAO;
    private static final Logger LOG = Logger.getLogger(UserController.class.getName());

    
    public void index() {
        LOG.info("UserController->index()");
        UserDAO UserDAO;     
        
        //objeto persistencia
        userDAO = new UserDAO();
        ArrayList<User> users = null;

        //leer datos de la persistencia
        synchronized (userDAO) {
            users = userDAO.getAll();
        }
        request.setAttribute("userList", users);
        String name = "index";
        LOG.info("En ModuleController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/user/index.jsp");        
    }
    
    
    public void create()
    {
        dispatch("/WEB-INF/view/user/create.jsp");
        LOG.info("UserController->create");
    }
    public void insert()
    {
        User user = new User();
        user.setName(request.getParameter("name"));
        user.setEmail(request.getParameter("email"));
        user.setPassword(request.getParameter("password"));
        user.setAge(Integer.parseInt(request.getParameter("age")));
        
        userDAO = new UserDAO();
        synchronized (userDAO) {
            userDAO.insert(user);
        }    
        redirect(contextPath + "/user/index?page=7");
    }
    
    
    
    public void store() throws SQLException {
        //crear objeto
        User user = loadFromRequest();

//        Book book = new Book();
//        book.setTitle(request.getParameter("title"));
//        book.setPublisher(request.getParameter("publisher"));
//        book.setPages(Integer.parseInt(request.getParameter("pages")));
//        book.setYear(Integer.parseInt(request.getParameter("year")));
        
        //objeto persistencia
        UserDAO userDAO;             
        userDAO = new UserDAO();
        
        //guardar objeto
        userDAO.insert(user);
        LOG.info("UserController->create");
        
        //redirigir
        redirect(contextPath + "/user/index");
        
    }
    public void delete(String idString) throws SQLException {
        LOG.info("UserController->index()");
        //objeto persistencia
        UserDAO userDAO;             
        userDAO = new UserDAO();
        userDAO.delete(Integer.parseInt(idString));
        //redirigir
        redirect(contextPath + "/user/index");

    }

    public void edit(String idString) throws SQLException {
        LOG.info("idString");
        long id = toId(idString);
        UserDAO  userDAO = new UserDAO();
        User user = userDAO.get((int) id);
        request.setAttribute("user", user);
        dispatch("/WEB-INF/user/edit.jsp");

    }

    public void update() throws IOException, SQLException {
        UserDAO  userDAO = new UserDAO();
        User user = loadFromRequest();
        //crear objeto
        user.setId(Integer.parseInt(request.getParameter("id")));
        
        userDAO.update(user);
        response.sendRedirect(contextPath + "/user/index");
        return;
    }
    
    
     private User loadFromRequest()
    {
        User user = new User();
        user.setName(request.getParameter("name"));
        user.setEmail(request.getParameter("email"));
        user.setPassword(request.getParameter("password"));
        user.setAge(Integer.parseInt(request.getParameter("age")));

        return user;
    }
            
}
