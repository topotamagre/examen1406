<%-- 
    Document   : create
    Created on : 12-jun-2017, 18:21:23
    Author     : usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Model.Book"%>
<jsp:useBean id="book" scope="request" class="Book" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Alta de libro</h1>
        <form method="POST" action="<%= request.getContextPath()%>/book/update">
            <input type="hidden" name="id" value="<%= book.getId()%>">
            <label>Título</label><input type="text" name="title" value="<%= book.getTitle() %>"><br>
            <label>Editorial</label><input type="text" name="publisher" value="<%= book.getPublisher()%>"><br>
            <label>Año</label><input type="text" name="year" value="<%= book.getYear()%>"><br>
            <label>Páginas</label><input type="text" name="pages" value="<%= book.getPages()%>"><br>
            <input type="submit" value="Guardar">
        </form>

    </body>
</html>
