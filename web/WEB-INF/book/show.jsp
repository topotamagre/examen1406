<%-- 
    Document   : show
    Created on : 12-jun-2017, 18:53:08
    Author     : usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Model.Book"%>
<jsp:useBean id="book" scope="request" class="Book" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Detalle del libro:</h1>
        <ul>
            <li>Título: <%= book.getTitle() %></li>
            <li>Editorial: <%= book.getPublisher()%></li>
            <li>Año: <%= book.getYear()%></li>
            <li>Páginas: <%= book.getPages()%></li>
        </ul>
    </body>
</html>
