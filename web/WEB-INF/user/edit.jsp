<%-- 
    Document   : edit
    Created on : 14-jun-2017, 18:47:50
    Author     : usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Model.User"%>
<jsp:useBean id="user" scope="request" class="User" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Edición de alumno</h1>
        <form method="POST" action="<%= request.getContextPath()%>/user/update">
            <input type="hidden" name="id" value="<%= user.getId()%>">
            <label>Nombre</label><input type="text" name="name" value="<%= user.getName() %>"><br>
            <label>Email</label><input type="text" name="email" value="<%= user.getEmail()%>"><br>
            <label>Password</label><input type="text" name="password" value="<%= user.getPassword()%>"><br>
            <label>Edad</label><input type="text" name="age" value="<%= user.getAge()%>"><br>
            <input type="submit" value="Guardar">
        </form>

    </body>
</html>
