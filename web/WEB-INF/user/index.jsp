<%-- 
    Document   : index
    Created on : 14-jun-2017, 18:42:56
    Author     : usuario
--%>


<%@page import="java.util.Iterator"%>
<%@page import="Model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:useBean id="userList" scope="request" class="java.util.ArrayList" />

    </head>
    <body>
        <h1>Lista de usuarios</h1>
        <p><a href="<%= request.getContextPath()%>/user/create">Nuevo</a></p>

        <table>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Password</th>
                <th>Año</th>
                <th>Acciones</th>
            </tr>
            <%
                Iterator<Model.User> iterator = userList.iterator();
                while (iterator.hasNext()) {
                    User item = iterator.next();%>
            <tr>
                <td><%= item.getId()%></td>
                <td><%= item.getName()%></td>
                <td><%= item.getEmail()%></td>
                <td><%= item.getPassword()%></td>
                <td><%= item.getAge()%></td>
                <td>
                    <a href="<%= request.getContextPath()%>/user/delete/<%= item.getId() %>">Borrar</a> 
                    <a href="<%= request.getContextPath()%>/user/edit/<%= item.getId() %>">Editar</a> 
                </td>
            </tr>
            <%
                }
            %>        
        </table>

    </body>
</html>